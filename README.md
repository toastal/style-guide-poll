# Style Guide Poll

Sometimes your coworkers bicker over coding standards. Rather than
adopting a full standard they don't like or trying to build on top of
one, create your own through democracy.

- - -

## Requirements

* [Git](http://git-scm.com/downloads)
* [Python 3.4+](https://www.python.org/downloads/) with `pvenv`

- - -

## Set Up

Clone develop branch
```bash
git clone -b develop git@bitbucket.org:toastal/style-guide-poll.git
```

Create a virtual environment out of the branch and activate
```bash
pyvenv-3.4 style-guide-poll
cd style-guide-poll
source bin/activate
```

Pip install the requirements
```bash
pip install -r requirements.txt
```

(Will likely not even use)
Django Smart Selects patch if needed (mind the python version) from the
root of the project
```bash
wget https://github.com/digi604/django-smart-selects/pull/51.patch
git apply --directory=lib/$(ls lib | head -1)/site-packages 51.patch
```

- - -

## Running

Activate and `cd` into the app directory (instructions above)
```bash
python manage.py runserver
```

Navigate to `0.0.0.0:8000` in a browser

- - -

## Checking pip updates

```bash
pip-review
```