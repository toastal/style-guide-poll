from django.contrib import admin

from polls.models import Language, StyleGuide, Poll, Style


class StyleInline(admin.StackedInline):
    model = Style
    extra = 1


class StyleAdmin(admin.ModelAdmin):
    filter_horizontal = ('style_guides',)
    list_filter = ['poll', 'style_guides']


class PollAdmin(admin.ModelAdmin):
    inlines = [StyleInline]
    list_filter = ['languages']
    search_fields = ['question']


admin.site.register(Language)
admin.site.register(StyleGuide)
admin.site.register(Poll, PollAdmin)
admin.site.register(Style, StyleAdmin)
