from datetime import timedelta

from django.db import models
from django.utils import timezone

# from smart_selects.db_fields import ChainedForeignKey, GroupedForeignKey
from markupfield.fields import MarkupField


class Language(models.Model):
    '''Languages are given a set of polls to vote on—they’re also on
    StyleGuides'''
    name = models.CharField(max_length=64, unique=True)

    def __str__(self):
        return self.name


class StyleGuide(models.Model):
    '''StyleGuides are trusted maintainer for style to inform voters'''
    name = models.CharField(max_length=64, unique=True)
    url = models.URLField(max_length=64)
    languages = models.ManyToManyField(Language)

    def __str__(self):
        return self.name


class Poll(models.Model):
    '''Polls are for each style rule'''
    question = models.CharField(max_length=256, unique=True)
    description = MarkupField(markup_type='markdown',
                              max_length=2000,
                              blank=True)
    languages = models.ManyToManyField(Language)
    pub_date = models.DateTimeField('date published', default=timezone.now())

    def __str__(self):
        return self.question

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - timedelta(days=1)


class Style(models.Model):
    '''Style are the possible options for a poll'''
    poll = models.ForeignKey(Poll)
    option = models.CharField(max_length=256)
    info = MarkupField(markup_type='markdown', max_length=2000, blank=True)
    style_guides = models.ManyToManyField(StyleGuide, blank=True)
    votes = models.IntegerField(default=0)  # editable=False

    class Meta:
        ordering = ('poll',)

    def __str__(self):
        return '{0}: {1} ({2})'.format(self.poll, self.option, self.votes)
